﻿# Express Checkout C# SDK

## Using .NET 4.5? 

All API calls to `https://api.juspay.in` must be over TLS1.2. In .NET 4.5 TLS1.2 support is not enabled
by default. To enable, update the `System.Net.ServicePointManager.SecurityProtocol` property.

```cs
System.Net.ServicePointManager.SecurityProtocol |= SecurityProtocolType.Tls12;
```

Refer: [https://stackoverflow.com/questions/28286086/default-securityprotocol-in-net-4-5](https://stackoverflow.com/questions/28286086/default-securityprotocol-in-net-4-5)

## Configure the SDK
Configure the SDK by invoking the `Juspay.ExpressCheckout.Base.Config.Configure` method:

```cs
using Juspay.ExpressCheckout.Base;

namespace MyApplication
{
    class MyClass
    {
        static MyClass()
        {
            Config.Configure(Config.Environment.SANDBOX, 
                "my-merchant-id", "my-api-key");
        }
    }
}
```

## API Usage

### General notes / Before starting

Ensure that the ExpressCheckout client is configured correctly by invoking the configuration logic. None of the
APIs described below will work if the configuration is not done / configuration is incorrect.

### Note on `ECApiResponse` and `async`
All APIs of the ExpressCheckout SDK return instances of `ECApiResponse`. An instance of `ECApiResponse` contains the following data:

- HTTP status code `StatusCode` of type `HttpStatusCode`
- Response headers `Headers` of type `HttpResponseHeaders`
- Response body `RawResponse` of type `string`
- Parsed response body `Response` of type `JObject`

Additionally note that all the APIs exposed are `async`

### Order APIs

##### Create Order
#
```cs
using Juspay.ExpressCheckout;
using Newtonsoft.Json.Linq;

namespace MyApplication 
{
    public class MyClass
    {
        public static async void CreateOrder()
        {
            // Prepare a dictionary of parameters.
            // the minimum 2 parameters required to create the order are the orderId and the order amount
            var OrderDetails = new Dictionary<string, string>();
            
            // Generate an order_id
            var OrderId = RandomOrderId();
            
            // Set the details
            OrderDetails.Add("order_id", OrderId);
            OrderDetails.Add("amount", "10.00");
            

            // Wait for the task to finish and
            ECApiResponse OrderResponse = await Orders.CreateOrder(OrderDetails);
        }
    }
}
```


##### Order Status API
#
```cs
using Juspay.ExpressCheckout;

namespace MyApplication
{
    class MyClass
    {
        public static async void FetchOrderStatus()
        {
            var OrderId = "Csharp-SDK-b2b78e5c-c2bf-481d-aae5-2003ec9738df";
            
            // Wait for the Task to finish and get the response
            ECApiResponse OrderStatusResponse = await Orders.GetStatus(OrderId);
        }
    }
}

```
##### Order List
#
```cs
namespace MyApplication
{
    class MyClass
    {
        public static async void GetOrderList()
        {
            int count = 20;
            
            // Wait for the Task to finish and get the response
            EcApiResponse OrderListResponse = await Orders.List(count);
        }
    }
}
```

##### Order Refund
#
```cs
namespace MyApplication
{
    class MyClass
    {
        public static async void RefundOrder()
        {
            // Pass the order id, the amount to be refunded and a unique identifier which is recorded in your system.
            EcApiReponse RefundResponse = await Order.Refund("my-order-id", 12.00, Guid.NewGuid().ToString());
        }
    }
}
```

### Customer APIs

##### Customer Create
#
```cs
namespace MyApplication
{
    class MyClass
    {
        public static async void CreateCustomer()
        {
            // The UniqueCustomerId must be generated by the merchant and should be provided when creating
            // the customer. This is later used to get / update the same customer.
            string UniqueCustomerId = String.format("customer_{0}", Guid.NewGuild().ToString());

            string CustomerMobileNumber = "9876543210";
            string EmailId = "customer@email.com"

            EcApiReponse CustomerCreateResponse = await Customer.CreateCustomer(UniqueCustomerId, 
                                                                                CustomerMobileNumber,
                                                                                EmailId);
        }
    }
}
```


##### Get Customer
#
```cs
namespace MyApplication
{
    class MyClass
    {
        public static async void GetCustomer()
        {
            // Unique customerId provided when creating the customer. This reference must be stored by you.
            string CustomerId = "my_customer_945b1fca-768e-430b-9115-eb6dbaf5b3f4";

            EcApiResponse CustomerDetails = await Customer.GetCustomer(CustomerId);
        }
    }
}
```

##### Edit Customer
```cs
namespace MyApplication
{
    class MyClass
    {
        public static async void UpdateCustomer()
        {
            // Unique customerId provided when creating the customer. This reference must be stored by you.
            string CustomerId = "my_customer_945b1fca-768e-430b-9115-eb6dbaf5b3f4";
            string FirstName = "Jon";
            string LastName = "Doe";
            string MobileNumber = "9876543210";

            EcApiresponse UpdateCustomerDetails = 
                await Customer.UpdateCustomer(CustomerId, 
                                              new Dictionary<string, string>() {
                                                    { "first_name", FirstName },
                                                    { "mobile_number", MobileNumber },
                                                    { "last_name", LastName } });
        }
    }
}
```